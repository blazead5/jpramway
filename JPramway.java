import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.*;

public class JPramway extends JFrame {
  private static DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
  private SearchBox stopField;
  private Client client;
  private JPanel resultPane;
  private JPramway() {
    client = new Client();
    setTitle("Pramway");
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    /* Main pane */ {
      var pane = getContentPane();
      pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
      /* Form */ {
        var form = new JPanel();
        var layout = new GroupLayout(form);
        form.setLayout(layout);
        form.setAlignmentX(Component.CENTER_ALIGNMENT);
        layout.setAutoCreateGaps(true);
        var stopLabel = new JLabel("Zastávka");
        stopField = new SearchBox();
        client.getStopNames()
            .thenAccept(stopNames -> stopField.setItems(stopNames))
            .exceptionally(err -> {
              err.printStackTrace();
              showText("Chyba při načítání seznamu zastávek ☹");
              return null;
            });
        layout.setHorizontalGroup(
            layout.createSequentialGroup().addComponent(stopLabel).addComponent(
                stopField));
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(stopLabel)
                .addComponent(stopField));
        pane.add(form);
      }
      /* Search button */ {
        var searchButton = new JButton("Vyhledat");
        searchButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        searchButton.addActionListener((event) -> {
          var index = stopField.getSelectedIndex();
          if (index == -1) {
            showText("Vyberte prosím název zastávky ze seznamu.");
          } else {
            var stopName = stopField.getItemAt(stopField.getSelectedIndex());
            showText("Načítání…");
            client.getDepartureBoard(stopName)
                .thenAccept(board -> showDepartureBoard(board))
                .exceptionally(err -> {
                  err.printStackTrace();
                  showText("Chybička se vloudila ☹");
                  return null;
                });
          }
        });
        pane.add(searchButton);
      }
      /* Result pane */ {
        resultPane = new JPanel();
        resultPane.setLayout(new BoxLayout(resultPane, BoxLayout.Y_AXIS));
        resultPane.setAlignmentX(Component.CENTER_ALIGNMENT);
        pane.add(resultPane);
      }
      pane.add(Box.createGlue());
    }
    setMinimumSize(new Dimension(600, 800));
    setVisible(true);
  }
  private void showText(String text) {
    resultPane.removeAll();
    resultPane.add(new JLabel(text));
    resultPane.revalidate();
    resultPane.repaint();
  }
  private static JPanel departurePane(DepartureBoard.Departure departure) {
    var pane = new JPanel();
    pane.setLayout(new BorderLayout());
    pane.add(new JLabel(String.format("<html><font size=+3>%s</font></html>",
                                      departure.route.short_name)),
             BorderLayout.LINE_START);
    var timestamp =
        departure.departure_timestamp.different()
            ? String.format(
                  "<s>%s</s> %s",
                  dateFormat.format(departure.departure_timestamp.scheduled),
                  dateFormat.format(departure.departure_timestamp.predicted))
            : dateFormat.format(departure.departure_timestamp.predicted);
    var badges = "";
    if (departure.route.is_substitute_transport) {
      badges += "🔀";
    }
    if (departure.trip.is_wheelchair_accessible) {
      badges += "♿";
    }
    if (departure.route.is_night) {
      badges += "🌙";
    }
    pane.add(
        new JLabel(String.format("<html>→ %s %s<br>%s</html>",
                                 departure.trip.headsign, badges, timestamp)),
        BorderLayout.CENTER);
    return pane;
  }
  private void showDepartureBoard(DepartureBoard board) {
    resultPane.removeAll();
    for (var departure : board.departures) {
      resultPane.add(departurePane(departure));
    }
    resultPane.revalidate();
    resultPane.repaint();
  }
  public static void main(String[] args) { new JPramway(); }
}
