import com.google.gson.Gson;
import java.io.*;
import java.net.*;
import java.net.http.*;
import java.net.http.HttpResponse.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class Client {
  private String apiKey;
  public Client() {
    try {
      apiKey = new String(Files.readAllBytes(Paths.get(".api-key"))).trim();
    } catch (IOException err) {
      err.printStackTrace();
    }
  }
  public CompletableFuture<DepartureBoard> getDepartureBoard(String stopName) {
    try {
      var client = HttpClient.newBuilder().build();
      var uri =
          new URI("https://api.golemio.cz/v2/pid/departureboards?names[]=" +
                  URLEncoder.encode(stopName, StandardCharsets.UTF_8));
      var request = HttpRequest.newBuilder()
                        .uri(uri)
                        .header("X-Access-Token", apiKey)
                        .GET()
                        .build();
      return client.sendAsync(request, BodyHandlers.ofString())
          .thenApply(
              response
              -> new Gson().fromJson(response.body(), DepartureBoard.class));
    } catch (URISyntaxException err) {
      throw new RuntimeException("Invalid URI");
    }
  }
  private static class StopsResult {
    public static class StopGroup { public String name; }
    public List<StopGroup> stopGroups;
  }
  public CompletableFuture<List<String>> getStopNames() {
    try {
      var client = HttpClient.newBuilder().build();
      var uri = new URI("https://data.pid.cz/stops/json/stops.json");
      var request = HttpRequest.newBuilder().uri(uri).GET().build();
      return client.sendAsync(request, BodyHandlers.ofString())
          .thenApply(response
                     -> new Gson()
                            .fromJson(response.body(), StopsResult.class)
                            .stopGroups.stream()
                            .map(stopGroup -> stopGroup.name)
                            .collect(Collectors.toList()));
    } catch (URISyntaxException err) {
      throw new RuntimeException("Invalid URI");
    }
  }
}
