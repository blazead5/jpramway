import java.util.Date;

// This class violates typical Java conventions (snake_case, public fields)
// in order to better map to the JSON structure.
public class DepartureBoard {
  public static class Departure {
    public static class Delay {
      public boolean is_available;
      public int minutes;
      public int seconds;
    }
    public Delay delay;
    public static class DepartureTimestamp {
      public Date predicted;
      public Date scheduled;
      public boolean different() { return !predicted.equals(scheduled); }
    }
    public DepartureTimestamp departure_timestamp;
    public static class Route {
      public String short_name;
      public int type;
      public boolean is_night;
      public boolean is_regional;
      public boolean is_substitute_transport;
    }
    public Route route;
    public static class Trip {
      public String headsign;
      public boolean is_wheelchair_accessible;
    }
    public Trip trip;
  }
  public Departure[] departures;
}
