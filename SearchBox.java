import java.awt.event.*;
import java.util.*;
import java.util.stream.Collectors;
import javax.swing.*;

// Implementation of a combobox whose selection is filtered based on the entered
// text.
public class SearchBox extends JComboBox<String> {
  private List<String> items;
  private JTextField textField;
  public void setItems(List<String> items_) {
    items = items_;
    update();
  }
  public SearchBox() {
    super();
    items = new ArrayList<String>();
    setEditable(true);
    textField = (JTextField)getEditor().getEditorComponent();
    textField.addKeyListener(new KeyAdapter() {
      public void keyReleased(KeyEvent event) { update(); }
    });
  }
  private void update() {
    var text = textField.getText();
    var textUpper = text.toUpperCase();
    List<String> searchResults =
        items.stream()
            .filter(item -> item.toUpperCase().contains(textUpper))
            .collect(Collectors.toList());
    // Matching items are sorted by the index of the matched string.
    Collections.sort(searchResults,
                     (item1, item2)
                         -> item1.toUpperCase().indexOf(textUpper) -
                                item2.toUpperCase().indexOf(textUpper));
    setModel(
        new DefaultComboBoxModel<String>(new Vector<String>(searchResults)));
    setSelectedItem(text);
    showPopup();
  }
}
