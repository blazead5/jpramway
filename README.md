**JPramway** je jednoduchá aplikace na hledání odjezdů v síti Pražské integrované dopravy. Byla vytvořena jako studentský projekt k předmětu Programování v Javě na FJFI ČVUT.

![Snímek obrazovky aplikace](screenshot.png)

## Spuštění

```sh
javac -cp '.:gson-2.10.1.jar' *.java
java -cp '.:gson-2.10.1.jar' JPramway.java
```

## Návod k použití

**Aplikace vyžaduje klíč pro Golemio API**, který je možné [získat zdarma](https://api.golemio.cz/api-keys). Klíč uložte do souboru s názvem `.api-key` v tomto adresáři.

K použití je nutné připojení k internetu. Zkompilujte a spusťte aplikaci pomocí příkazů výše. Do vyhledávacího pole zadejte název zastávky (po získání seznamu zastávek se budou automaticky nabízet možnosti). Po kliknutí na tlačítko Vyhledat se zobrazí nejbližší odjezdy všech linek z vybrané zastávky. Jsou zahrnuty informace o aktuálním zpoždění a bezbariérové přístupnosti.

## Použité technologie

- **Swing**: grafické rozhraní
- **Golemio API**: získávání dat od PID
- **GSON**: deserializace formátu JSON
